# Site d'enseignement / Portfolio

Voici le site d'introduction à mes enseignements.

C'est également un exemple d'utilisation des pages Framagit (*GitLab Pages*) au travers de la réalisation
d'un thème pour le générateur de sites statiques [Pelican].

Le thème est adapté du thème [freelancer], lui-même adapté du
thème de [Bootstrap].
> Le thème Pelican a subi de nombreuses modifications, dont la migration en Bootstrap 4.

Le tout est une démonstration de la réalisation d'un portfolio pour
les étudiants en BTS SIO.


## GitLab CI

Les pages statiques du projet sont générés par [GitLab CI][ci], dont voici
la configuration au travers du fichier [`.gitlab-ci.yml`](.gitlab-ci.yml):

```
image: python:3-alpine

pages:
  script:
  - pip install -r requirements.txt
  - pelican -s publishconf.py
  artifacts:
    paths:
    - public/
```

## Travailler en local

Après avoir téléchargé ou cloné le projet, vous pouvez installer le site statique de plusieurs façons.

### Dans un environnement virtuel Python
Lancez d'abord la commande :
```
pip install -r requirements.txt
```
Exécutez ensuite le serveur de développement :
```
make devserver
```
Le site est alors disponible à l'adresse `http://localhost:8000/`.

### Sur une distribution GNU/Linux
Il est nécessaire d'installer le paquet **Pelican**, par exemple sur une distribution Debian, en exécutant la commande :
```
apt install pelican
```
Exécutez ensuite le serveur de développement :
```
make devserver
```

Le site est alors disponible à l'adresse `http://localhost:8000/`.
Lire plus d'informations sur la [documentation] de Pelican.


## Notes sur la personnalisation du thème [freelancer]

Certaines des couleurs du thème d'origine ont été personnalisées :
* `#1abc9c` -> `#92c83e`
* `#159a80`, `#148f77`, `#117a8b`, `#117964` -> `#689029`

L'image du profil a été arrondie:

```
.masthead .masthead-avatar {
  width: 15rem;
  border-radius: 100%;
}
```

## Licence
* Les fichiers thème *Freelancer - Start Bootstrap* sont disponibles sous la licence [mit].
* Les fichiers de code, notamment les fichiers *template* sont disponibles sous la licence [agpl].
* La paternité de l'image favicon (vélo) n'est pas revendiquée (source à retrouver).
* Sauf mention contraire, le contenu est disponible sous [licence CC-BY-SA][ccbysa] version 3.0 ou ultérieure.

[freelancer]: https://github.com/ondoheer/freelancer-theme-pelican/
[bootstrap]: https://startbootstrap.com/themes/freelancer/
[ci]: https://about.gitlab.com/gitlab-ci/
[pelican]: http://blog.getpelican.com/
[documentation]: http://docs.getpelican.com/
[mit]: https://github.com/StartBootstrap/startbootstrap-freelancer/blob/master/LICENSE
[agpl]: http://www.gnu.org/licenses/agpl-3.0.html
[ccbysa]: https://creativecommons.org/licenses/by-sa/3.0/fr/

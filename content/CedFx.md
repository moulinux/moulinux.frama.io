Title: CedFx
Date: 2020-08-21 12:20
Image: CedFx_1.png

La calculette éco-déplacement est une application qui permet d’évaluer
les impacts environnementaux et économiques des différents modes de transport
et permet de comparer les résultats entre deux modes de déplacement pour un même trajet.

![Comparaison](./img/CedFx_1.png)

Cette application est utilisée dans le cadre de ma progression pédagogique
avec les BTS SIO. Il existe plusieurs versions. Cette version repose sur le
*framework* JavaFX.

### Introduction
Cette application est inspirée de celle de l'[Ademe], qui n'était cependant pas libre
et encore moins interopérable (reposant notamment dans sa première version sur Flash).
Cette version de l'application propose de nombreuses itérations (accessibles via les différentes
branches du dépôt). L'itération principale n'est pas la plus aboutie, mais rend
la démonstration plus simple car les déplacements ajoutés, le sont dans une collection
en mémoire (et non sérialisés dans une base de données relationnelles).

![Déplacements](./img/CedFx_2.png)

### Intégration dans la progression
Cette version de l'application accompagne les étudiants SLAM dans leur
découverte de la librairie **JavaFX**, qui permet d'implémenter des applications
multi-plateformes en **Java**.
Cette étude se déroule lors du troisième semestre.

Les différentes itérations sont disponibles sous [licence GPL][gpl],
le [code source][cedfx] est disponible sur la forge logicielle Framagit.

[Ademe]: https://www.ademe.fr/
[gpl]: http://www.gnu.org/licenses/gpl-3.0.html
[cedfx]: https://framagit.org/ced/CedFx

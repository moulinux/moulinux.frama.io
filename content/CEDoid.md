Title: CEDoid
Date: 2020-08-21 12:00
Image: CEDoid_0-list.png

L'application permet de calculer l'impact de nos déplacements quotidiens sur l'environnement et nos dépenses.

La calculette éco-déplacement permet d’évaluer les impacts environnementaux et économiques des différents modes de transport et permet de comparer les résultats entre deux modes pour un même trajet.

L'application permet de démontrer que l’usage du vélo et de la marche ont un impact positif en termes de dépenses, d’émission de gaz à effet de serre et de consommation d’énergie toujours en comparaison avec les autres modes de transports.

![Liste](./img/CEDoid_1-list.png)

### ADEME
Cette application est inspirée de l'éco-comparateur de l'[Ademe].
Malheureusement, cette application n'étant pas libre,
nous ne pouvons pas en étudier le code source pour en comprendre les bases de la programmation.

Cette version va bien au délà, puisqu'en plus de proposer une version
mobile, elle s'interface sur l'API CEDfony.

### Intégration dans la progression
Les différentes itérations de cette application accompagnent la découverte
de la programmation avec le *framework* Android.

Cette fois-ci l'application n'est pas le support de la progression, mais sert
de questionnement tout au long de la progression. C'est pourquoi cette
version de l'application n'est pas publique.

![Ajout](./img/CEDoid_2-add.png)

L'application permet d'ajouter de nouveaux déplacements.

![Infos](./img/CEDoid_3-infos.png)

![Initialisation](./img/CEDoid_4-init.png)

Une entrée dans le menu permet de synchroniser l'application mobile sur
l'API REST, propulsée par CEDfony.

[Ademe]: https://www.ademe.fr/

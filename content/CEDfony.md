Title: CEDfony
Date: 2020-08-21 12:10
Image: CEDfony.png

CEDfony est une API REST écrit en PHP avec le *framework* **Symfony**.

L'application permet d'évaluer les impacts environnementaux de différents modes de transport.

L'API permet les opérations [CRUD] sur les différents modes de transport.
Elle permet également aux utilisateurs d'enregistrer un déplacement, en
précisant la distance quotidienne parcourue, dans un mode de transport
défini, entre le domicile et le lieu de travail.

![API](./img/CEDfony.png)

### ADEME
Cette application est inspirée de l'éco-comparateur de l'[Ademe].
Elle va cependant bien au delà, puisqu'à ma connaissance cette application
ne dispose pas d'API (c'est-à-dire d'interface de programmation).

### Intégration dans la progression
L'application sert de support à l'introduction aux API REST en deuxième année.

Les différentes itérations de cette application permettent de découvrir
le développement progressif d'une application PHP avec le *framework*
**Symfony**. Jusque-là, les étudiants ont travaillé, notamment en AP
avec l'application AMAphp, sur des applications *prêtent à l'emploi*.

Les différentes itérations sont disponibles sous [licence GPL][agpl],
le [code source][cedfony] est disponible sur la forge logicielle Framagit.

[CRUD]: https://fr.wikipedia.org/wiki/CRUD
[Ademe]: https://www.ademe.fr/
[agpl]: http://www.gnu.org/licenses/agpl-3.0.html
[cedfony]: https://framagit.org/moulinux/CEDfony

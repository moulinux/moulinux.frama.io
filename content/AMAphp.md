Title: AMAphp
Date: 2020-08-21 12:30
Image: AMAphp.png

Cette situation professionnelle prend place dans le contexte AMAP.
Dans ce cadre, les étudiants travaillent pour le compte de la société 2lo.
Le client est le réseau AMAP-Auvergne.

![Accueil AMAphp](./img/AMAphp.png "AMAphp")

### Le prestataire de services : La société 2lo
Créée en 2000, 2lo (logiciel libre pour les organisations) n’a cessé d’enrichir
son équipe en nouvelles compétences et expertises afin de proposer à ses clients
des solutions toujours plus innovantes.
2Lo est spécialisée dans le développement logiciel, l'ingénierie réseau
et l'hébergement web. La principale activité de la société consiste à
proposer des solutions d'hébergements sur des serveurs dédiés. Les employés
de la société possèdent également une expertise dans l’intégration de services,
le développement logiciel et la gestion/création de bases de données.

### Le client : Le réseau AMAP Auvergne
Depuis la création des premières AMAP en Auvergne en 2006, elles se sont
multipliées à un rythme exponentiel portant à 30 le nombre de groupes en ce début 2013,
soit environ 1500 familles d’amapiens travaillant avec 80 paysans partenaires.
Basé sur des partenariats innovants et responsables entre paysans et consommateurs,
le contrat AMAP est devenu un levier politique et citoyen pour qu'une autre
dynamique agricole puisse exister à travers des circuits courts et des engagements environnementaux.

Mais si l'offre de « circuits courts » a littéralement explosé (un agriculteur
sur cinq vend aujourd'hui en circuit court), le mouvement des AMAP représente une
véritable opportunité pour l’agriculture régionale : loin de s'en tenir à
l'ouverture de nouveaux débouchés commerciaux, le principe du contrat AMAP
repose sur des principes de solidarité, de salaire décent pour les paysans partenaires,
de transparence, de respect de l'environnement et de proximité.

Le **réseau des AMAP Auvergne** est la structure régionale de ce mouvement.
Si sa mission principale demeure la promotion et l’aide à la création d’AMAP,
il mobilise aujourd'hui une bonne partie de son énergie à multiplier des actions
visant à favoriser l’installation de jeunes paysans partenaires en impliquant
également les groupes de citoyens et les collectivités locales.
Par ailleurs, il assure l’animation et la structuration de son réseau en tant
que lieu d’échange, d’accompagnement et de mutualisation des pratiques amapiennes.

### La situation professionnelle
Le réseau AMAP Auvergne souhaite disposer d'un outil permettant aux AMAP adhérentes
du réseau de gérer au quotidien les paysans partenaires et leurs contrats.
L'application AMAphp répond à cette demande. Ce gestionnaire d'AMAP est écrit
en PHP avec le *framework* **Symfony**.

![Liste paysans](./img/AMAphp1a_1.png "AMAphp1a-1")
Liste des paysans partenaires.

![Liste contrats](./img/AMAphp1a_2.png "AMAphp1a-2")
Les contrats en cours entre les mangeurs et les paysans partenaires
(la durée du contrat est précisée dans cette vue récapitulative).

> Cette application est partagée sous une licence éducative propriétaire,
afin de se prémunir d'un partage incontrôlé des versions modifiées.

### Intégration dans la progression
L'étude de la base de données de l'application sert de support à un TP SQL
pour les étudiants SLAM (Bloc 2 du référentiel).

Les étudiants suivant la spécialité SLAM (Solutions Logicielles et Applications Métiers)
travailleront à faire évoluer cette application dans le cadre de missions
réalisées en AP (ateliers de professionnalisation).

Dans une première mission, au second semestre, les étudiants doivent résoudre
des incidents déjà repérés sur la version 1a de l'application. Cette version
de l'application repose sur une classe PDO pour l'accès aux données.

Dans une seconde mission, au troisième semestre, les étudiants doivent faire
évoluer l'application (plusieurs projets en parallèle). Ils travaillent cette
fois-ci à partir de la version 2c de l'application.

![Authentification](./img/AMAphp2c_1.png "AMAphp2c-1")

Cette itération repose notamment sur Doctrine DBAL et les symfony forms.
Une couche de **sécurité** a été implémentée. Une authentification est désormais
nécessaire pour accéder à une grande partie du contenu. Les autorisations
sont réparties dans deux catégories : utilisateur et administrateur.

![Liste contrats](./img/AMAphp2c_2.png "AMAphp2c-2")

![Modification mangeur](./img/AMAphp2c_3.png "AMAphp2c-3")

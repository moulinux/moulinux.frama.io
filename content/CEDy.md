Title: CEDy
Date: 2020-08-21 13:00
Image: CEDy_5.png

L'application permet de calculer l'impact de nos déplacements quotidiens sur l'environnement et nos dépenses.

La calculette éco-déplacement permet d’évaluer les impacts environnementaux et économiques des différents modes de transport et permet de comparer les résultats entre deux modes pour un même trajet.

L'application permet de démontrer que l’usage du vélo et de la marche ont un impact positif en termes de dépenses, d’émission de gaz à effet de serre et de consommation d’énergie toujours en comparaison avec les autres modes de transports.

### ADEME
Cette application est inspirée de l'éco-comparateur de l'[Ademe].
Malheureusement, cette application n'étant pas libre,
nous ne pouvons pas en étudier le code source pour en comprendre les bases de la programmation.

### Intégration dans la progression
Nous travaillons sur plusieurs itérations de cette application. La première itération,
en ligne de commande, de l'application ne prend par exemple en charge aucune saisie.
Ces différentes itérations accompagnent la progression.

![CLI](./img/CEDy_5.png "CEDy-5")

L'itération finale propose une interface graphique écrite avec la librairie
**Tkinter** (une autre version met en œuvre le framework **Kivy**).

![Tkinter](./img/CEDy_7.png "CEDy-7")

Le développement de cette application s'inscrit dans le bloc n°1 du référentiel SIO.
Le langage de programmation de support est le **python**.

Les différentes itérations sont disponibles sous [licence GPL][gpl],
le [code source][cedy] est disponible sur la forge logicielle Framagit.

[Ademe]: https://www.ademe.fr/
[gpl]: http://www.gnu.org/licenses/gpl-3.0.html
[cedy]: https://framagit.org/ced/CEDy

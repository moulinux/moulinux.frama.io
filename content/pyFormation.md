Title: pyFormation
Date: 2020-08-21 12:25
Image: pyFormation_3-list-sessions.png

Cette situation professionnelle prend place dans le contexte Formation SNCF.

L’organisation concernée par cette étude, la SNCF a une mission de service public de transport que chacun a, un jour ou l’autre, utilisé et apprécié.
Au-delà du transport des passagers et des marchandises, la SNCF gère en interne plus de 450 métiers qui concourent à sa bonne marche.

Des personnels aux compétences rares côtoient ainsi des employés ayant des qualifications techniques usuelles et indispensables : du gestionnaire du patrimoine immobilier au soudeur spécialisé inox, en passant par la police des gares, l’ingénieur de conception ou l’agent d’accueil.

À Bordeaux, dans un vaste bâtiment hébergeant la Direction Régionale, le service Formation du Personnel, situé hiérarchiquement au sein de la Direction des Ressources Humaines, participe aux projets de formations et satisfait les demandes et les suivis de formations des salariés de la région Aquitaine - Poitou - Charentes. En 2004, ce sont plus de 350 000 heures de formation qui y ont été assurées.

![Accueil pyFormation](./img/pyFormation_0-menu.png "Menu pyFormation")

L'application pyFormation dispose d'une documentation technique qui a
été générée avec Sphinx, elle est accessible [en ligne][documentation]
(via les pages de framagit et l'intégration continue).

### Objectifs
À partir de l’observation des programmes de l’application en ligne de
commandes, pyFormation, les étudiants doivent :

* Observer les insuffisances dans le contrôle de validité des informations saisies;
* Proposer des solutions pour garantir la concordance des informations saisies
via le gestionnaire de tickets de la forge logicielle;
* Compléter ou créer une procédure de façon à prendre en charge ces nouveaux besoins,
puis intégrer la modification à la gestion de version du projet.

#### écran activité
![Liste des activités](./img/pyFormation_1-list-activites.png "Activités pyFormation")

#### écran action de formation
![Liste des actions](./img/pyFormation_2-list-actions.png "Actions pyFormation")

#### écran sessions
![Liste des sessions](./img/pyFormation_3-list-sessions.png "Sessions pyFormation")

### Intégration dans la progression
Cette activité s’inscrit dans les ateliers de professionnalisation (AP)
du second semestre pour les étudiants ayant opté pour la spécialité SLAM
(Solutions Logicielles et Applications Métiers).

Cette situation professionnelle place les étudiants en situation d’acteurs
au sein d’un processus de support. Ils doivent mettre en œuvre les notions
acquises au premier semestre, afin d’identifier des problèmes (incidents)
et de proposer des pistes d’amélioration du service rendu.

### Compétences travaillées
Durant cette mission, les étudiants ont notamment l’occasion de travailler certaines des compétences suivantes :

* B1.1.1 : Recenser et identifier les ressources numériques
* B1.1.2 : Exploiter des référentiels, normes et standards adoptés par le prestataire informatique
* B1.2.1 : Collecter, suivre et orienter des demandes
* B1.2.3 : Traiter des demandes concernant les applications
* B1.4.1 : Analyser les objectifs et les modalités d’organisation d’un projet

[documentation]: https://slam.frama.io/formation/pyFormation_0-0/

Title: CEDjs
Date: 2020-08-21 12:50
Image: CEDjs.png

L'application permet de calculer l'impact de nos déplacements quotidiens sur l'environnement et nos dépenses.

La calculette éco-déplacement permet d’évaluer les impacts environnementaux et économiques des différents modes de transport et permet de comparer les résultats entre deux modes pour un même trajet.

L'application permet de démontrer que l’usage du vélo et de la marche ont un impact positif en termes de dépenses, d’émission de gaz à effet de serre et de consommation d’énergie toujours en comparaison avec les autres modes de transports.

### ADEME
Cette application est inspirée de l'éco-comparateur de l'[Ademe].
Malheureusement, cette application n'étant pas libre,
nous ne pouvons pas en étudier le code source pour en comprendre les bases de la programmation.

### Intégration dans la progression
Le travaille sur cette version **javascript** de l'application éco-déplacement
se situe à la suite de la progression sur l'application CEDy (le pendant
**python** de cette version de l'application).

![Interface](./img/CEDjs.png "CEDjs")

Là encore, nous travaillons sur plusieurs itérations de l'application.
La première itération permet de découvrir les bases du développement Web.
Les itérations suivantes permettent de découvrir progressivement les
bases du langage **javascript**.

Le développement de cette application s'inscrit également dans le bloc n°1 du référentiel SIO.

Les différentes itérations sont disponibles sous [licence GPL][gpl],
le [code source][cedjs] est disponible sur la forge logicielle Framagit.

[Ademe]: https://www.ademe.fr/
[gpl]: http://www.gnu.org/licenses/gpl-3.0.html
[cedjs]: https://framagit.org/ced/CEDjs

#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'moulinux'
SITENAME = 'Moulinux'
DESCRIPTION = 'Dev, Développement application, BTS, SIO, Python, Java, JavaFX, Php, GNU/Linux, Android, PostgreSql, Kivy, Django, Symfony, Enseignement, Éducation'
SITEURL = ''

PATH = 'content'
OUTPUT_PATH = 'public'

TIMEZONE = 'Europe/Paris'

DEFAULT_LANG = 'fr'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('Pelican', 'http://getpelican.com/'),
         ('Python.org', 'http://python.org/'),
         ('Jinja2', 'http://jinja.pocoo.org/'),
         ('You can modify those links in your config file', '#'),)

# Social widget
SOCIAL = (('You can add links in your config file', '#'),
          ('Another social link', '#'),)

#DEFAULT_PAGINATION = 6

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

# Personnalisation SM
SITESUBTITLE = 'Enseignant - Développement Python, Java, PHP - GNU/Linux, Android'

THEME = 'themes/freelancer-theme-pelican'
#THEME_STATIC_DIR = 'static'
STATIC_PATHS = ( "img/", )

# Liens du menu du haut
NAVLINKS = (
	('#page-top', ''),
	('#portfolio', 'Mes réalisations'),
	('#about', 'À propos'),
	('#contact', 'Contact')
)

# Nom Portfolio
PORTFOLIO = 'Mes réalisations'

# Formulaire de contact
CONTACT_FIELDS = (
	['Nom', 'text', 'name', 'Entrez votre nom.'],
	['Addresse mail', 'email', 'email','Saisissez votre adresse e-mail.' ],
	['Téléphone', 'tel', 'phone', 'Saisissez votre numéro de téléphone.'],
	['Message', 'textarea', 'message', 'Saisissez votre message.']
)
